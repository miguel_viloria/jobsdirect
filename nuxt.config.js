import pkg from './package'

export default {
  env: {
    firebaseInitializer: {
      apiKey: "AIzaSyDXecKYj4Mjm_rE0iMwVmA5FINxJqEgkZA",
      authDomain: "jobsdirect-50898.firebaseapp.com",
      databaseURL: "https://jobsdirect-50898.firebaseio.com",
      projectId: "jobsdirect-50898",
      storageBucket: "jobsdirect-50898.appspot.com",
      messagingSenderId: "172680849849"
    },
    city: [
      "",
      "Quezon",
      "Marikina",
      "Makati",
      "Pasay",
      "Rizal",
      "San Mateo",
      "Quiapo",
      "Cebu",
      "San Lazaro",
      "Fairview",
      "Metro Manila",
      "Taguig",
      "Mandaluyong",
      "San Juan",
      "Pasig",
      "Paranaque",
      "Muntinlupa"
    ],
    education: [
      "",
      "Elementary",
      "Intermediate",
      "Collegiate"
    ],
    gender: [
      "",
      "Male",
      "Female",
      "Other"
    ]
  },
  mode: 'spa',
  server: {
    host: '0.0.0.0', // default: localhost
  },
  generate: {
    dir: 'www'
  },

  router: {
    mode: 'hash'
  },
  /*
  ** Headers of the page
  */
  head: {
    title: pkg.name,
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: pkg.description }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },

  /*
  ** Global CSS
  */
  css: [
  ],

  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '~/plugins/onsen',
    '~/plugins/cordova'
  ],

  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
  ],
  /*
  ** Axios module configuration
  */
  axios: {
    // See https://github.com/nuxt-community/axios-module#options
  },

  /*
  ** Build configuration
  */
  build: {
    publicPath: '/nuxt/',
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {
      config.module.rules.push({ test: /\.coffee$/, loader: 'coffee-loader' })
      config.module.rules.push({ test: /\.yml$/, loader: 'yaml-loader' })
      config.resolve.extensions.push('.coffee')
    }
  }
}
